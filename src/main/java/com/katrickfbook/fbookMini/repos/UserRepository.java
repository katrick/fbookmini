package com.katrickfbook.fbookMini.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.katrickfbook.fbookMini.entities.User;

@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User, Long> {

	
}
