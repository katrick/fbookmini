package com.katrickfbook.fbookMini.Iservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import javax.activity.InvalidActivityException;
import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.katrickfbook.fbookMini.entities.User;
import com.katrickfbook.fbookMini.repos.UserRepository;
import com.katrickfbook.fbookMini.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;
	
	@Override
	public User createUser(User user) {
		
		user = userRepo.save(user);
		return user;
	}

	@Override
	public User addFriend(String userId, String friendId) throws InvalidActivityException {
		
		User currentUser = userRepo.findById(Long.parseLong(userId)).get();
		User friendUser = userRepo.findById(Long.parseLong(friendId)).get();
		String friendList = currentUser.getFriendList();
		String[] s = friendList.split(",");
		
		Set<Integer> friendSet = new HashSet<>();
		
		for(String str : s) {
			if(str.length()>0) {
				int fId = Integer.parseInt(str);
				friendSet.add(fId);
			}
		}
		
		if(!friendSet.contains(Integer.parseInt(friendId))) {
			String sep_string = ",";
			if(friendList.length()==0) {
				sep_string = "";
			}else {
				sep_string = ",";
			}
			friendList = friendList + sep_string + friendId;
			currentUser.setFriendList(friendList);
			currentUser = userRepo.save(currentUser);
			
			String frndListStr = friendUser.getFriendList();
			if(frndListStr.length()==0) {
				sep_string = "";
			}else {
				sep_string = ",";
			}
			frndListStr = frndListStr + sep_string + userId;
			friendUser.setFriendList(frndListStr);
			userRepo.save(friendUser);
		}else {
			throw new InvalidActivityException("They Are already Friends");
		}
		
		return currentUser;
	}
	
	@Override
	public User removeFriend(String userId, String friendId) throws InvalidActivityException {
		
		User currentUser = userRepo.findById(Long.parseLong(userId)).get();
		User friendUser = userRepo.findById(Long.parseLong(friendId)).get();
		String friendList = currentUser.getFriendList();
		String[] s = friendList.split(",");
		
		String cUserNewFlist = "";
		for(String str : s) {
			if(str.equalsIgnoreCase(friendId)) {
				continue;
			}
			String sep_string = ",";
			if(cUserNewFlist.length()==0) {
				sep_string = "";
			}else {
				sep_string = ",";
			}
			cUserNewFlist = cUserNewFlist + sep_string + str;
		}
		currentUser.setFriendList(cUserNewFlist);
		currentUser = userRepo.save(currentUser);
		
		String friendUserFList = friendUser.getFriendList();
		String[] fArray = friendUserFList.split(",");
		
		String fUserNewFlist = "";
		for (String str : fArray) {
			if(str.equalsIgnoreCase(userId)) {
				continue;
			}
			String sep_string = ",";
			if(fUserNewFlist.length()==0) {
				sep_string = "";
			}else {
				sep_string = ",";
			}
			fUserNewFlist = fUserNewFlist + sep_string + str;
		}
		friendUser.setFriendList(fUserNewFlist);
		userRepo.save(friendUser);
		
		return currentUser;
	}

	@Override
	public User getUserFromDB(Long id) {
		
		
		User op = userRepo.findById(id).get();
		if(op.getEmail() != null) {
			return op;
		}
		
		return null;
	}
	
	@Override
	public Map<String,Object> getUserFriendListFromDB(Long id) {
		
		List<User> userFriendList = new ArrayList<>();
				
		User op = userRepo.findById(id).get();
		String fListString = op.getFriendList();
		String[] arrStr = fListString.split(",");
		
		for(String s : arrStr) {
			User u = userRepo.findById(Long.parseLong(s)).get();
			userFriendList.add(u);
		}
		Map<String,Object> friendMap = new HashMap<>();
		if(userFriendList.size() > 0) {
			friendMap.put("data",userFriendList);
			return friendMap;
		}else {
			friendMap.put("data",null);
		}
		
		return friendMap;
	}

	@Override
	public Map<String, Object> getFriendListAtDistance(String userId, String distanceValue) {
		
		Queue<Node> q = new LinkedList<>();
		Node root = new Node(Long.parseLong(userId));
//		root.visited = 1;
		q.offer(root);
		
		Queue<Node> qf = new LinkedList<>();
		int k = Integer.parseInt(distanceValue);
		int t = 0;
		
		List<Long> visitedNodesIdList = new ArrayList<>();
		while(!q.isEmpty()) {
			if(t==k) {
				break;
			}
			Node curr = q.poll();
			if(curr.visited == -1) {
				curr.visited = 1;
				visitedNodesIdList.add(curr.data);
				User u = userRepo.findById(curr.data).get();
				String fString = u.getFriendList();
				String[] strArray = fString.split(",");
				
				for(String g : strArray) {
					if(!visitedNodesIdList.contains(Long.parseLong(g))) {
						Node fnode = new Node(Long.parseLong(g));
//						fnode.visited = 1;
						qf.offer(fnode);
					}
				}
				
				if(q.isEmpty()) {
					t++;
					q.addAll(qf);
					qf.clear();
				}
			}
			
		}
		
		List<User> userList = new ArrayList<>();
		
		for(Node z : q) {	
			User usr = userRepo.findById(z.data).get();
			userList.add(usr);
		}
		
		Map<String,Object> responseMap = new HashMap<>();
		
		if(userList.size() > 0) {
			responseMap.put("data", userList);
		}else {
			responseMap.put("data", null);
		}
		
		return responseMap;
	}
	
	class Node{
		long data;
		int visited;
		
		public Node(long d) {
			data = d;
			visited = -1;
		}

		@Override
		public String toString() {
			return "Node [data=" + data + ", visited=" + visited + "]";
		}
	}

}
