package com.katrickfbook.fbookMini.controllers;

import java.util.Map;
import java.util.Optional;

import javax.activity.InvalidActivityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.katrickfbook.fbookMini.entities.User;
import com.katrickfbook.fbookMini.repos.UserRepository;
import com.katrickfbook.fbookMini.services.UserService;

import antlr.collections.List;

@RestController
public class CreateProfileRESTController {

	@Autowired
	UserRepository userRepo;
	@Autowired
	UserService userService;
	
	@GetMapping("user/{id}")
	public User findUser(@PathVariable("id") Long id) {
		
		User u = userService.getUserFromDB(id);
		if(u!= null) {
			return u;
		}
		return null;
	}
	
	@GetMapping("user/{id}/friendlist")
	public Map<String,Object> getFriendList(@PathVariable("id") Long id) {
		
		Map<String,Object> u = userService.getUserFriendListFromDB(id);
		if(u!= null) {
			return u;
		}
		return null;
	}
	
	@PostMapping("user")
	public User registerUser(@RequestBody User user) {
		
		if(user.getName() != null && user.getEmail() != null && user.getPhone() != null) {
			user = userService.createUser(user);
			return user;
		}else {
			throw new NullPointerException("Provide All required fields");
		}
		
	}
	
	@PostMapping("add/friend")
	public User addToFriendList(@RequestBody Map<String,Object> requestParam) throws InvalidActivityException {
		
		Object userId = requestParam.get("userId");
		Object friendId = requestParam.get("friendId");
		
		if(requestParam != null && requestParam.get("userId") != null && requestParam.get("friendId") != null) {
			User response = userService.addFriend(userId.toString(),friendId.toString());
			return response;
		}else {
			throw new NullPointerException("Provide All required fields");
		}
	}
	
	@PostMapping("remove/friend")
	public User removeFromFriendList(@RequestBody Map<String,Object> requestParam) throws InvalidActivityException {
		
		Object userId = requestParam.get("userId");
		Object friendId = requestParam.get("friendId");
		
		if(requestParam != null && requestParam.get("userId") != null && requestParam.get("friendId") != null) {
			User response = userService.removeFriend(userId.toString(),friendId.toString());
			return response;
		}else {
			throw new NullPointerException("Provide All required fields");
		}
	}
	
	@PostMapping("friends/mutual")
	public Map<String,Object> getMutualFriendList(@RequestBody Map<String,Object> requestParam) throws InvalidActivityException {
		
		Object userId = requestParam.get("userId");
		Object distanceValue = requestParam.get("distance");
		
		if(requestParam != null && requestParam.get("userId") != null && requestParam.get("distance") != null) {

			Map<String,Object> friendList  = userService.getFriendListAtDistance(userId.toString(),distanceValue.toString());
			return friendList;
		} else {
			throw new NullPointerException("Provide All required fields");
		}
	}
}
