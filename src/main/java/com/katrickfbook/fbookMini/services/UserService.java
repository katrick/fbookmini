package com.katrickfbook.fbookMini.services;

import java.util.List;
import java.util.Map;

import javax.activity.InvalidActivityException;

import com.katrickfbook.fbookMini.entities.User;

public interface UserService {

	public User createUser(User user);

	public User addFriend(String string, String string2) throws InvalidActivityException;

	User removeFriend(String userId, String friendId) throws InvalidActivityException;

	public User getUserFromDB(Long id);

	Map<String,Object> getUserFriendListFromDB(Long id);

	public Map<String, Object> getFriendListAtDistance(String userId, String distanceValue);
}
